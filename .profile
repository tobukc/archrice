#!/bin/sh

# Profile file. Runs on login.

################################################
# Environment Variables per XDG specifications
################################################
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export SRC_HOME="/src"
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME/notmuch/notmuch-config"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export LESSHISTFILE="-"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export INPUTRC="$XDG_CONFIG_HOME/inputrc"
export GNUPGHOME="$XDG_DATA_HOME/gnupg/current"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/password-store"
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/startup.py"
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"
export NODE_REPL_HISTORY="$XDG_DATA_HOME/node_repl_history"
export SQLITE_HISTORY="$XDG_DATA_HOME/sqlite_history"
export OCTAVE_HISTFILE="$XDG_CACHE_HOME/octave-hsts"
export OCTAVE_SITE_INITFILE="$XDG_CONFIG_HOME/octave/octaverc"
# docker-compose support for podman
export DOCKER_HOST="unix://$XDG_RUNTIME_DIR/podman/podman.sock"
export NVM_DIR="$XDG_CONFIG_HOME/nvm"
export VAGRANT_HOME="$XDG_DATA_HOME/vagrant"
export VAGRANT_ALIAS_FILE="$XDG_DATA_HOME/vagrant/aliases"
export ANDROID_SDK_HOME="$XDG_CONFIG_HOME/android"
export ANSIBLE_CONFIG="$XDG_CONFIG_HOME/ansible/ansible.cfg"
export GOPATH="$XDG_DATA_HOME/go"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export AWS_CONFIG_HOME="$XDG_CONFIG_HOME/aws"
export AWS_VAULT_BACKEND="pass"
export AWS_VAULT_PASS_PREFIX="aws-vault"
export AWS_VAULT_PASS_PASSWORD_STORE_DIR="$PASSWORD_STORE_DIR"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export SDKMAN_DIR="$XDG_DATA_HOME/sdkman"
# cloud foundary
export CF_HOME="$XDG_CONFIG_HOME/cf"
export CF_PLUGIN_HOME="$CF_HOME/plugins"

# Set path
export PATH="$PATH:$(find -L "$HOME/.local/bin/" -type d | paste -sd ':'):$(find -L "$HOME/.local/opt/bin/" -type d | paste -sd ':')"

export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="fox"
export READER="zathura"

# HiDPI settings
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export QT_SCALE_FACTOR=1.5
export GDK_DPI_SCALE=1.3
#export GDK_SCALE=2

# less/man colors
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"

# other program settings
export QT_QPA_PLATFORMTHEME="gtk2"	# Have QT use gtk2 theme.
export MOZ_USE_XINPUT2="1"		# Mozilla smooth scrolling/touchpads.
export _JAVA_AWT_WM_NONREPARENTING=1	# Java doesn't understand tiling windows
export AWT_TOOLKIT="MToolkit wmname LG3D"	#May have to install wmname

# email setup
export MAILCAP=$XDG_CONFIG_HOME/mutt/mailcap:/usr/share/mutt-wizard/mailcap:/usr/share/neomutt/mailcap:/etc/mailcap:/etc/mailcap:/usr/etc/mailcap:/usr/local/etc/mailcap

# pass copy-setting
export PASSWORD_STORE_X_SELECTION="clipboard"

[[ -f "$HOME/.local/bin/shortcuts" ]] && "$HOME/.local/bin/shortcuts"

# Start graphical server if Xorg not already running.
[ "$(tty)" = "/dev/tty1"  ] && ! pgrep -x Xorg >/dev/null && exec startx
