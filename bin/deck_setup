#!/bin/bash

# options for better error handling
set -Eeuo pipefail

# reinstall the following packages due to missing files
echo "Reinstalling certain system packages due to missing files..."
sudo pacman -S \
  glibc \
  linux-api-headers

echo "Installing additional packages..."
sudo pacman -S \
  --overwrite /etc/vifm/colors/Default-256.vifm \
  --overwrite /etc/containers/registries.conf.d/00-shortnames.conf \
  --overwrite /etc/containers/registries.d/default.yaml \
  cronie \
  fzf \
  vi \
  neovim \
  python-pynvim \
  python-virtualenv \
  ripgrep \
  fd \
  jq \
  nodejs \
  yarn \
  tmux \
  powerline \
  powerline-fonts \
  pass \
  xclip \
  flameshot \
  vifm \
  alacritty \
  ttf-liberation \
  ttf-linux-libertine \
  podman \
  podman-docker \
  networkmanager-fortisslvpn \
  xcape \
  xorg-xmodmap \
  nsxiv \
  zathura \
  zathura-pdf-mupdf \
  cifs-utils \
  libreoffice-still \

# configure rootless podman
echo "Configuring rootless podman..."
sudo pacman -S fuse-overlayfs slirp4netns
podman system reset
podman system migrate

echo "Completed [Please reboot!]"
