setlocal wrap
setlocal linebreak
setlocal nolist
setlocal showbreak=…
setlocal wrapmargin=0
setlocal textwidth=0
setlocal spell
setlocal spell spelllang=en_us
setlocal spellfile=~/.local/share/spellfile.add

highlight clear SpellBad
highlight SpellBad term=standout ctermfg=1
highlight SpellBad term=underline cterm=underline
highlight clear SpellCap
highlight SpellCap term=underline cterm=underline
highlight clear SpellRare
highlight SpellRare term=underline cterm=underline
highlight clear SpellLocal
highlight SpellLocal term=underline cterm=underline

" Spell-check settings for email
nmap <F8> :w<CR>:!aspell -e -c %<CR>:e<CR>

" Goyo
" Must initialize lightline before automatic Goyo
" https://github.com/junegunn/goyo.vim/issues/207
autocmd WinEnter,BufEnter * call lightline#init()
autocmd WinEnter,BufEnter * Goyo
