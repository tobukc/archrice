" Turn off spellcheck in javascript comments
syntax region  jsComment        start=+//+ end=/$/ contains=jsCommentTodo,@NoSpell extend keepend
syntax region  jsComment        start=+/\*+  end=+\*/+ contains=jsCommentTodo,@NoSpell fold extend keepend
