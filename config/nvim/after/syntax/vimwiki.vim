" ignore any words mixed with uppercase and digits
syntax match uppercaseWord "\v<\u+(\d+\L*)?(s?)>" contains=@NoSpell

" ignore any single char
syntax match singleChar "\v<[a-zA-Z]>" contains=@NoSpell

" allow lower case after i.e. or e.g.
syntax match forExample "\v<(i\.e\.)|(e\.g\.)\s<\l+>" contains=@NoSpell

