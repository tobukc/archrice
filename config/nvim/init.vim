"=====================================================================
" My Plugins
" Specify a directory for plugins
" - For Neovim: ~/.config/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
"=====================================================================
if ! filereadable(expand('~/.config/nvim/autoload/plug.vim'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ~/.config/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ~/.config/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.config/nvim/plugged')

" Make sure you use single quotes

Plug 'junegunn/goyo.vim'

" The basic fzf vim plugin is already included within the package and installed to
" Vim's global plugin directory.
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-commentary'
Plug 'jiangmiao/auto-pairs'
Plug 'airblade/vim-rooter'
Plug 'adelarsq/vim-matchit'
Plug 'jeetsukumaran/vim-indentwise'
Plug 'christoomey/vim-tmux-navigator'

" wiki and notes
Plug 'vimwiki/vimwiki'

" Custom text objects
Plug 'wellle/targets.vim'
Plug 'wellle/line-targets.vim'
Plug 'michaeljsmith/vim-indent-object'

" Motion plugins (similar to the 'f' motion but works on the entire file)
Plug 'easymotion/vim-easymotion'

" Colors & Status Line
Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'
Plug 'arcticicestudio/nord-vim'
Plug 'joshdick/onedark.vim'

" Development
Plug 'sheerun/vim-polyglot/'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'honza/vim-snippets'
Plug 'hashivim/vim-terraform'

" Utils
Plug 'godlygeek/tabular'

" Initialize plugin system
call plug#end()

"=====================================================================
" Basic Settings
"=====================================================================

set nocompatible

" set file encoding
scriptencoding utf-8

" set file format
set fileformat=unix
set fileformats=unix,mac,dos

" automatic indent
set autoindent

" set terminal's title
set title

" turn on the line number
set number

" turn on relative number
set relativenumber

" shift width
set sw=2

" fix backspace problem
set bs=2

" tab width
set tabstop=2

" number of white spaces to delete when backspace
set softtabstop=2

" use space for tab key
set expandtab

" show parentheses match
set showmatch

" always turn on syntax highlighting
syntax on

" turn on/off search pattern highlight
"set hlsearch
set hlsearch

set incsearch
set ignorecase
set smartcase

" vim command history
set history=1000

" turn on wildmenu
set wildmenu

" use emacs-style tab completion when selecting files, etc
set wildmode=longest,list,full

set ttimeoutlen=50        " Make Esc work faster
"set printoptions=paper:letter

" disable auto backup and swap file (use git please)
set nobackup
set noswapfile

" number of lines before start scrolling
set scrolloff=5

" turn on hidden for better buffer management
set hidden

" always turn on sign column
set signcolumn=yes

" Set characters for invisibles
set listchars=tab:→\ ,eol:¬

" Enable omni completion
set omnifunc=syntaxcomplete#Complete

" Splits open at the bottom and right, which is non-retarded,
" unlike vim defaults.
set splitbelow
set splitright

" Turn on cursor line
set cursorline

" Turn on support for clipboard (xclip)
" Sync system clipboard with the default yank register
set clipboard+=unnamedplus

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Color Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" nord settings
let g:nord_uniform_status_lines = 1

" set color scheme
colorscheme onedark

"highlight cursor line in current window only
augroup CursorLine
  au!
  autocmd VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  autocmd WinLeave * setlocal nocursorline
augroup END

" visual color
highlight Visual cterm=NONE ctermbg=Yellow ctermfg=Black

" omni completion popup menu
highlight Pmenu cterm=NONE ctermbg=Yellow ctermfg=Black
highlight PmenuSel ctermbg=DarkBlue ctermfg=Yellow

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" GUI Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if has("gui_running")
  " window size
  set lines=70
  set columns=220

  " no menu or toolbar
  "set guioptions-=m
  set guioptions-=T

  " disable all scrollbars
  set guioptions-=l
  set guioptions-=r
  set guioptions-=b
  set guioptions-=R " the right hand scrollbar for vertically split window
  set guioptions-=L " the left hand scrollbar for vertically split window

  " set font
  if has("win32") || has("win64")
    set guifont=Bitstream_Vera_Sans_Mono:h9:cANSI
  elseif os == "Darwin"
    set guifont=Droid\ Sans\ Mono\ for\ Powerline:h12
  endif

  if has("gui_macvim")
    " enable alt key as meta key on mac
    set macmeta
  endif
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Windows Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if has('nvim')
	" Terminal mode key mappings
  tnoremap <M-;> <C-\><C-n>
  " Terminal mode:
  tnoremap <M-h> <c-\><c-n><c-w>h
  tnoremap <M-j> <c-\><c-n><c-w>j
  tnoremap <M-k> <c-\><c-n><c-w>k
  tnoremap <M-l> <c-\><c-n><c-w>l
  tnoremap <M-q> <c-\><c-n><c-w>c
  tnoremap <M-w> <c-\><c-n><c-w><c-w>
  " Insert mode:
  inoremap <M-h> <Esc><c-w>h
  inoremap <M-j> <Esc><c-w>j
  inoremap <M-k> <Esc><c-w>k
  inoremap <M-l> <Esc><c-w>l
  inoremap <M-q> <Esc><c-w>c
  inoremap <M-w> <Esc><c-w><c-w>
  " Visual mode:
  vnoremap <M-h> <Esc><c-w>h
  vnoremap <M-j> <Esc><c-w>j
  vnoremap <M-k> <Esc><c-w>k
  vnoremap <M-l> <Esc><c-w>l
  vnoremap <M-q> <Esc><c-w>c
  vnoremap <M-w> <Esc><c-w><c-w>
  " Normal mode:
  nnoremap <M-h> <c-w>h
  nnoremap <M-j> <c-w>j
  nnoremap <M-k> <c-w>k
  nnoremap <M-l> <c-w>l
  nnoremap <M-q> <c-w>c
  nnoremap <M-w> <c-w><c-w>
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Syntax Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" set groovy specific syntax options
let groovy_allow_cpp_keywords = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Custom key mapping
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" change the mapleader from \ to ,
let mapleader=","
noremap \ ,

" press ESC to turn off highlighting and clear any message already displayed
" and escape
nnoremap <silent> <esc> :nohlsearch<Bar>:echo<CR><ESC>

" press space to forward a page
nnoremap <silent> <Space> <C-f>

" Force yourself to stop using arrow keys
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" quick save in normal and insert mode
inoremap <C-s> <C-O>:w<CR>
nnoremap <C-s> :w<CR>
nnoremap <leader>s :w<CR>

" Swap ` to '
nnoremap ' `
nnoremap ` '

" Copy from cursor to the end of the line
nnoremap Y y$

" Go to the beginning/end of line in insert mode
inoremap <C-a> <Esc>I
inoremap <C-e> <Esc>A

" Add a new line in insert mode
inoremap <C-r> <Esc>A<CR>

" copy and paste with xclip
vnoremap <leader>xy "+y
nnoremap <leader>xp "+p
nnoremap <leader>xP "+P

" Treat W as w in command mode but only if it is neither
" followed nor preceded by word characters.
cnoreabbrev W w

" Map j and k to gj and gk to work around long lines
" with line wrapping enabled. gj and gk only move
" one line in the editor.
nnoremap j gj
nnoremap k gk

" scroll up and down
nnoremap L Lzt
nnoremap H HzbH

" Use Q for formatting the current paragraph or selection
vnoremap Q gq
nnoremap Q gqap

" Select the last changed text (or the text that was just pasted)
nnoremap gV `[v`]

" Select the indent level and lines above/below
" This depends on 'michaeljsmith/vim-indent-object' plugin
vmap V <ESC>vaI

" Show or hide whitespaces
nmap <leader>L :set list!<CR>

" Split windows horizontally and vertically
map <C-W>- <C-W><C-S>
map <C-W>\| <C-W><C-V>

" Jump to the window below and maximize the window
map <C-W>j <C-W>j<C-W>_

" Jump to the window above and maximize the window
map <C-W>k <C-W>k<C-W>_

" Easy diffget on a 3-way diff
nnoremap dgh :diffget //2 \| diffupdate <CR>
nnoremap dgl :diffget //3 \| diffupdate <CR>

" Switching to the previously edited buffer
map <leader><Space> :b#<CR>

" Close current buffer
map <leader>d :bd<CR>
" Close current buffer without saving
map <leader>D :bd!<CR>
" Close and save current buffer
map <leader>W :w \| bd<CR>

" Edit vimrc
map <leader>ve :edit $MYVIMRC<CR>
" Reload vimrc
map <leader>vr :source $MYVIMRC<CR>

" Check file in shellcheck (need to install shellcheck first)
map <leader>S :!clear && shellcheck %<CR>

" Search and replace the word under cursor
nnoremap <leader>r :%s/\<<C-r><C-w>\>/

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" File Type Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Enable filetype-specific indenting
filetype indent on

" Enable filetype-specific plugins
filetype plugin on

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" indent-wise Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap [[ [%
nmap ]] ]%

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Easymotion Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Move to char
map s <Plug>(easymotion-bd-f)
nmap s <Plug>(easymotion-overwin-f)

" Move to word
nmap S <Plug>(easymotion-overwin-w)

" Move to line
nmap <leader>l <Plug>(easymotion-overwin-line)

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Fzf Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" open fzf
nnoremap <silent> <leader>f :Files!<CR>
" open sibling files using fzf
"nnoremap <silent> <leader>- :Files <C-r>=expand("%:h")<CR>/<CR>
" open fzf buffers
nnoremap <silent> <leader>b :Buffers!<CR>
" navigate files by fuzzy tag
nnoremap <silent> <leader>t :BTags!<CR>
nnoremap <silent> <leader>T :Tags!<CR>
" search by lines
nnoremap <silent> <Leader>/ :BLines!<CR>
" rg
nnoremap <silent> <Leader>? :Rg!<CR>
" marks
nnoremap <silent> <Leader>m :Marks!<CR>
" search edit history
nnoremap <silent> <Leader>H :History!<CR>

" overwrite the grep command
command! -bang -nargs=* Rg
  \ call fzf#vim#grep("rg --hidden --ignore-file ~/.config/ripgrep/rgignore --column --line-number --no-heading --color=always --smart-case -- ".shellescape(<q-args>), 1,
  \ fzf#vim#with_preview(), <bang>0)

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vimwiki Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:vimwiki_list = [
  \{'path': '~/.local/share/wiki/kb', 'name': 'knowledge-base', 'syntax': 'markdown', 'ext': '.md', 'nested_syntaxes': { 'javascript': 'javascript', 'sh': 'sh' }},
  \{'path': '~/.local/share/wiki/personal', 'name': 'personal', 'syntax': 'markdown', 'ext': '.md'},
  \{'path': '~/.local/share/wiki/math', 'name': 'math', 'syntax': 'markdown', 'ext': '.md'}
\]

" disable the default table mappings in insert mode
" custom table mappings are defined in ftplugin
let g:vimwiki_table_mappings=0

" Quick view - vim key mappings
map gh :tab sview ~/.local/share/wiki/kb/vim/key-mappings.md<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Markdown Preview Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:vim_markdown_preview_pandoc=1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Goyo Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Goyo plugin makes text more readable when writing prose:
map <leader>G :Goyo \| set linebreak<CR>

" Custom goyo callback functions
" Exit goyo when quitting vim
function! Goyo_before()
  let b:quitting = 0
  let b:quitting_bang = 0
  autocmd QuitPre <buffer> let b:quitting = 1
  cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
endfunction

function! Goyo_after()
  " Quit Vim if this is the only remaining buffer
  if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
    if b:quitting_bang
      qa!
    else
      qa
    endif
  endif
endfunction

let g:goyo_callbacks = [function('Goyo_before'), function('Goyo_after')]

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Targets Settings (Text Objects)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Change the last target shortcut key to 'N'
let g:targets_nl = ['n', 'N']

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Lightline Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" turn off mode indicator
" because mode is included in status line
set noshowmode

" always display status line
set laststatus=2

" always show tabline
set showtabline=2

let g:lightline#bufferline#show_number = 2
let g:lightline#bufferline#unnamed = '[No Name]'
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'cocstatus', 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'tabline': {
      \   'left': [ ['buffers'] ],
      \   'right': [ ['close'] ]
      \ },
      \ 'component_expand': {
      \   'buffers': 'lightline#bufferline#buffers'
      \ },
      \ 'component_type': {
      \   'buffers': 'tabsel'
      \ },
      \ 'component_function': {
      \   'cocstatus': 'coc#status',
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }

nmap <Leader>1 <Plug>lightline#bufferline#go(1)
nmap <Leader>2 <Plug>lightline#bufferline#go(2)
nmap <Leader>3 <Plug>lightline#bufferline#go(3)
nmap <Leader>4 <Plug>lightline#bufferline#go(4)
nmap <Leader>5 <Plug>lightline#bufferline#go(5)
nmap <Leader>6 <Plug>lightline#bufferline#go(6)
nmap <Leader>7 <Plug>lightline#bufferline#go(7)
nmap <Leader>8 <Plug>lightline#bufferline#go(8)
nmap <Leader>9 <Plug>lightline#bufferline#go(9)
nmap <Leader>0 <Plug>lightline#bufferline#go(10)

nmap <Leader>c1 <Plug>lightline#bufferline#delete(1)
nmap <Leader>c2 <Plug>lightline#bufferline#delete(2)
nmap <Leader>c3 <Plug>lightline#bufferline#delete(3)
nmap <Leader>c4 <Plug>lightline#bufferline#delete(4)
nmap <Leader>c5 <Plug>lightline#bufferline#delete(5)
nmap <Leader>c6 <Plug>lightline#bufferline#delete(6)
nmap <Leader>c7 <Plug>lightline#bufferline#delete(7)
nmap <Leader>c8 <Plug>lightline#bufferline#delete(8)
nmap <Leader>c9 <Plug>lightline#bufferline#delete(9)
nmap <Leader>c0 <Plug>lightline#bufferline#delete(10)

nmap <Leader>h1 :split <bar> :call lightline#bufferline#go(1)<CR>
nmap <Leader>h2 :split <bar> :call lightline#bufferline#go(2)<CR>
nmap <Leader>h3 :split <bar> :call lightline#bufferline#go(3)<CR>
nmap <Leader>h4 :split <bar> :call lightline#bufferline#go(4)<CR>
nmap <Leader>h5 :split <bar> :call lightline#bufferline#go(5)<CR>
nmap <Leader>h6 :split <bar> :call lightline#bufferline#go(6)<CR>
nmap <Leader>h7 :split <bar> :call lightline#bufferline#go(7)<CR>
nmap <Leader>h8 :split <bar> :call lightline#bufferline#go(8)<CR>
nmap <Leader>h9 :split <bar> :call lightline#bufferline#go(9)<CR>
nmap <Leader>h0 :split <bar> :call lightline#bufferline#go(0)<CR>

" Rooter Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:rooter_patterns = ['tags', '.git', '.git/', '_darcs/', '.hg/', '.bzr/', '.svn/']

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Auto-Pairs Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" unmap return key to avoid conflict with coc
let g:AutoPairsMapCR = 0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ShowMarks Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:showmarks_enable=0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vim-Fugitive Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Leader + gs: Git status
map <silent> <leader>gs :Gstatus<CR>

" Command aliases for Fugitive
command! Gs Gstatus
command! Gc Gcommit
command! Gw Gwrite
command! Gr Gread
command! Gmv Gmove
command! Grm Gremove

" mapping to write commit and push to current branch
nnoremap gwp :call PushToCurrentBranch()<CR>

" By Endel Dreyer
" Write COMMIT_EDITMSG and push to current branch
function! PushToCurrentBranch()
  exe ":Gwrite"
  let branch = fugitive#statusline()
  let branch = substitute(branch, '\c\v\[?GIT\(([a-z0-9\-_\./:]+)\)\]?',
  $BRANCH.' \1', 'g')
  exe ":Git push origin" . branch
endfunction

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Coc Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" use system node for coc
let g:coc_node_path = '/usr/bin/node'

" coc extensions
let g:coc_global_extensions = [
  \ 'coc-emoji', 'coc-eslint', 'coc-snippets', 'coc-tsserver',
  \ 'coc-css', 'coc-json', 'coc-cfn-lint']

" Custom snippets are stored in ./config/nvim/coc/ultisnips
" To activate custom snippets, syslink the custom snippet
" directory to $XDG_CONFIG_HOME/coc/ultisnips

" Use <C-l> for trigger snippet expand.
imap <C-l> <Plug>(coc-snippets-expand)

" Use <C-j> for select text for visual placeholder of snippet.
vmap <C-j> <Plug>(coc-snippets-select)

" Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-j>'

" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<c-k>'

" Use <C-j> for both expand and jump (make expand higher priority.)
imap <C-j> <Plug>(coc-snippets-expand-jump)

" Use <leader>x for convert visual selected code to snippet
xmap <leader>x  <Plug>(coc-convert-snippet)

let g:coc_snippet_next = '<tab>'

" Use <cr> to confirm complete
inoremap <silent><expr> <cr> coc#pum#visible() ? coc#_select_confirm() : "\<C-g>u\<CR>"

" use <tab> to trigger completion and navigate to the next complete item
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"

" Use `[c` and `]c` for navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Search snippets
nnoremap <silent> <leader>n :CocList snippets<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Terraform Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:terraform_align = 1

" format terraform code on save
let g:terraform_fmt_on_save = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Auto commands
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Change root directory on buffer enter
autocmd BufEnter * :Rooter

" Automatically deletes all trailing whitespace on save.
autocmd BufWritePre * %s/\s\+$//e

" Run xrdb whenever Xresources is updated.
autocmd BufWritePost ~/.config/Xresources !xrdb %

" Close HTML/XML tags automatically
autocmd FileType html,xml,xsl,jsp,jtpl,gsp,gtpl,ejs,jst,javascript,javascriptreact source ~/.config/nvim/scripts/closetag.vim

" Use ;; to add ; at the end of the line in insert mode
autocmd FileType javascript,javascriptreact,java,sql,matlab :inoremap ;; <C-o>A;

" Load Shopify liquid files as javascript
autocmd BufNewFile,BufRead *.js.liquid set syntax=javascript
