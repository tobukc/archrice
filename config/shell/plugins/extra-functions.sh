# dictionary lookup using oxford api
# usage: def [phrase]
def() {
  curl -s -H 'app_id: 5a81524a' -H 'app_key: 0b9f2d9fe24734789b62b7dd7c73638d' "https://od-api.oxforddictionaries.com/api/v2/entries/en-us/$1" | jq 'if (.error) then .error else .results[].lexicalEntries[].entries[].senses[0].definitions[] end'
}

# dictionary lookup using cdict.net
dict() {
  $BROWSER "https://cdict.net/?q=$1"
}

# generate random password using gpg
# usage: pw [length]
pw() {
  gpg --armor --gen-random 1 256 | head -c $1 | tr '/' '#' | tr '+' '$' && echo ""
}
