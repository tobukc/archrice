#!/bin/sh
IFS='
'

TARGET_DIR=$1
[ -d "$TARGET_DIR" ] || { >&2 echo "Could not find the target directory."; exit 1; }

SRC_DIR=$(realpath $(dirname $0))
SRC_CONFIG_DIR="$SRC_DIR/config"
SRC_DATA_DIR="$SRC_DIR/share"

TARGET_CONFIG_DIR="$TARGET_DIR/.config"
TARGET_LOCAL_DIR="$TARGET_DIR/.local"
TARGET_BIN_DIR="$TARGET_LOCAL_DIR/bin"
TARGET_DATA_DIR="$TARGET_LOCAL_DIR/share"

# config folders and files
[ -d "$TARGET_CONFIG_DIR" ] || mkdir -p "$TARGET_CONFIG_DIR"
config_items=($(ls -1 $SRC_CONFIG_DIR))
for i in "${config_items[@]}"
do
  rm -f "$TARGET_CONFIG_DIR/$i" && ln -s "$SRC_CONFIG_DIR/$i" "$TARGET_CONFIG_DIR/$i"
done

# local folders
[ -d "$TARGET_DATA_DIR" ] || mkdir -p "$TARGET_DATA_DIR"
rm -rf $TARGET_BIN_DIR && ln -s "$SRC_DIR/bin" $TARGET_BIN_DIR

# data folders
data_items=($(ls -1 $SRC_DATA_DIR))
for i in "${data_items[@]}"
do
  rm -f "$TARGET_DATA_DIR/$i" && ln -s "$SRC_DATA_DIR/$i" "$TARGET_DATA_DIR/$i"
done

# home files
home_items=($(find $SRC_DIR -maxdepth 1 -type f \( ! -name ".gitignore" ! -name "deploy*.sh" \) -exec basename {} \;))
for i in "${home_items[@]}"
do
  rm -f "$TARGET_DIR/$i" && ln -s "$SRC_DIR/$i" "$TARGET_DIR/$i"
done
[ -f "$SRC_DIR/.profile" ] && rm -f "$TARGET_DIR/.zprofile" && ln -s "$SRC_DIR/.profile" "$TARGET_DIR/.zprofile"
